# Image raiz
FROM node

# Carpeta raiz
WORKDIR /apitechu

# Copia de archivos de carpeta local a apitechu
ADD . /apitechu

# Instalacioin de las dependencias
RUN npm install --only=prod

# Puerto de trabajo
EXPOSE 3000

# Comando de inicializacion
CMD ["node" , "server.js"]
