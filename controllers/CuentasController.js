
const io = require('../io');
const crypt = require('../crypt')
const requestJson = require('request-json');
const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechucmf12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function createCuentaV2(req, res) {
  console.log("POST /apitechu/v2/altamovimiento")
  console.log(req.body);
  console.log(req.body.iban);
  console.log(req.body.id);
  console.log(req.body.balance);
  req.body.id = req.body.id * (1)

                   var newCuenta = {
                     "iban" : req.body.iban,
                     "id" : req.body.id,
                     "balance" : req.body.balance
                    }
                    var httpClient = requestJson.createClient(baseMLABUrl);
                    httpClient.post("cuentas?" + mLabAPIKey, newCuenta,
                      function(err,resMlab, body){
                        if (err){
                          var response = {
                            "msg" : "error generando cuenta"
                          }
                          res.status(500)
                          res.send(response)
                        } else {
                            console.log("cuenta creada")
                            var response = {
                              "msg" : "cuenta registrada"
                            }
                            res.status=(201)
                            res.send(response)
                        }
                      }
                      )
            }


function actualizaBalanceV2(req, res){
  console.log("POST /apitechu/v2/cuentas");
  var iban = req.body.iban;
  var balance = req.body.balance;
  console.log("la cuenta  a actualizr es " + iban)
  var putBody = '{"$set":{"balance":' + balance + '}}';
  var query = 'q={"iban":' + '"' + iban + '"' + '}';
  console.log("la query " + query)
  var httpClient = requestJson.createClient(baseMLABUrl);
  httpClient.put("cuentas?" + query + "&" + "m=true" + "&" + mLabAPIKey, JSON.parse(putBody),
      function(err,resMlab, body){
        if (err){
          var response = {
            "msg" : "error obteniendo cuentas"
          }
          res.status(500)
          res.send(response)
        } else {

            var response = {
              "msg" : "balance actualizado"
            }
            res.status(201)
            res.send(response)

      }
  }
)
}








function getCuentasByIdV2(req, res){
  console.log("GET /apitechu/v2/cuentas/:id");
  var id = req.params.id;
  console.log("la id del usuario a obtener es " + id)
  var query = 'q={"id":' + id + '}';
  console.log("la query es" + query);
  var httpClient = requestJson.createClient(baseMLABUrl);
  httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
  function(err,resMlab, body){
//    var response = !err ? body[0] : {
//       "msg" : "error obteniendo usuarios"
     if (err){
       var response = {
         "msg" : "error obteniendo usuario"
       }
       res.status(500)
     } else {
       if(body.length > 0) {
         var response = body;
       } else {
         var response = {
           "msg" : "usuario no encontrado"
         }
         res.status(404)
       }
      }

    res.send(response)
  }
)
}





module.exports.createCuentaV2 = createCuentaV2;
module.exports.getCuentasByIdV2 = getCuentasByIdV2;
module.exports.actualizaBalanceV2 = actualizaBalanceV2;
