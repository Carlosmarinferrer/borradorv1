
const io = require('../io');
const crypt = require('../crypt')
const requestJson = require('request-json');
const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechucmf12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function createMovimientoV2(req, res) {
  console.log("POST /apitechu/v2/movimientos")
  console.log(req.body);
  console.log(req.body.descripcion);
  console.log(req.body.importe);
  console.log(req.body.fecha);
  if (req.body.descripcion == "Reintegro") {
    req.body.importe = req.body.importe * (-1)
  }
                   var newUser = {
                     "fecha" : req.body.fecha,
                     "hora" : req.body.hora,
                     "descripcion" : req.body.descripcion,
                     "importe" : req.body.importe,
                     "iban": req.body.iban,
                     "id" : 1
                    }
                    var httpClient = requestJson.createClient(baseMLABUrl);
                    httpClient.post("movimientos?" + mLabAPIKey, newUser,
                      function(err,resMlab, body){
                        if (err){
                          var response = {
                            "msg" : "error generando movimiento"
                          }
                          res.status(500)
                          res.send(response)
                        } else {
                            console.log("movimiento creado")
                            var response = {
                              "msg" : "movimiento registrado"
                            }
                            res.status=(201)
                            res.send(response)
                        }
                      }
                      )
            }

function getMovimientosByIdIbanV2(req, res){
  console.log("GET /apitechu/v2/cuentas/:id/:iban");
  var id = req.params.id;
  var iban = req.params.iban
  console.log("la id del usuario a obtener es " + id)
    console.log("la cuenta del usuario a obtener es " + iban)

  var query = 'q={"iban":' + '"'  + iban + '"' + '}';
  console.log("la query es" + query);
  var httpClient = requestJson.createClient(baseMLABUrl);
  httpClient.get("movimientos?" + query + "&" + mLabAPIKey,
  function(err,resMlab, body){
//    var response = !err ? body[0] : {
//       "msg" : "error obteniendo usuarios"
     if (err){
       var response = {
         "msg" : "error obteniendo usuario"
       }
       res.status(500)
     } else {
       if(body.length > 0) {
         var response = body;
         res.send(response)
       } else {
         var response = {
           "msg" : "usuario no encontrado"
         }
         res.status(404)
         res.send(response)
       }
      }
  }
)
}

module.exports.getMovimientosByIdIbanV2= getMovimientosByIdIbanV2;
module.exports.createMovimientoV2= createMovimientoV2;
