require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

const cuentasController = require('./controllers/CuentasController');
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const movimientosController = require('./controllers/MovimientosController');
var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 // This will be needed.
 res.set("Access-Control-Allow-Headers", "*");

 next();
}
app.use(express.json());
app.use(enableCORS);
app.listen(port);
console.log("API escuchado en el puerto b" + port);

// app.get("/apitechu/v1/hello",
//   function (req,res){
//     console.log("GET /apitechu/v1/hello");
//     res.send({ "msg":"hola desde api"});
// }
// )

// app.post("/apitechu/v1/monstruo/:p1/:p2",
// function(req,res) {
//   console.log("parametros");
//   console.log(req.params);
//   console.log("query string");
//   console.log(req.query);
//   console.log("headers");
//   console.log(req.headers);
//   console.log("body");
//   console.log(req.body);
// }
// )
// app.get("/apitechu/v1/users",
// function(req, res) {
//   console.log("GET /apitechu/v1/users");
//   var users = require('./usuarios.json');
//   res.sendFile("usuarios.json", {root:__dirname});
//     res.send(users);
// }
//
// )


//app.get("/apitechu/v1/users/", userController.getUsersV1);
app.post("/apitechu/v2/logout/", userController.deleteAuthV2);
app.get("/apitechu/v2/users/", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);
app.get("/apitechu/v2/cuentas/:id", cuentasController.getCuentasByIdV2);
app.get("/apitechu/v2/movimientos/:id/:iban", movimientosController.getMovimientosByIdIbanV2);
app.post("/apitechu/v2/users/", userController.createUserV2);
app.post("/apitechu/v2/altacuenta/", cuentasController.createCuentaV2);

app.post("/apitechu/v2/movimientos/", movimientosController.createMovimientoV2);
app.post("/apitechu/v2/balance/", cuentasController.actualizaBalanceV2);

//app.post("/apitechu/v1/login/", authController.createAuthV1);
app.post("/apitechu/v2/login/", userController.createAuthV2);
//app.post("/apitechu/v2/alta/", userController.createUserV2);
//app.post("/apitechu/v1/logout/:id", authController.deleteAuthV1);


//app.post("/apitechu/v1/logout/:id", authController.deleteAuthV1);



//app.delete("/apitechu/v1/users/:id",userController.deleteUserV1);
